<div align="center" >
  <img src="./assets/Logo-Digital-Innovation-One-Site.jpg" width="200px">
  <img src="./assets/kotlin-hero.svg" width="100px">
  <h1> Introdução ao Kotlin </h1>
</div>


<div>

## Índice

 * [Ferramentas](#ferramentas)
 * [Tipos de Dados](#tipos-de-dados)
 * [Declaração de Variáveis](#declaração-de-variáveis)
 * [Valor nulo e Operadores aritméticos básicos](#valor-nulo-e-operadores-aritméticos-básicos)
   * [Nullability](#nullability)
   * [Operadores Aritméticos](#operadores-aritméticos) 
 * [Operadores Comparativos](#operadores-comparativos)
 * [Operadores lógicos e Operadores In e Range](#operadores-lógicos-e-operadores-in-e-range)
   * [Operadores Lógicos](#operadores-lógicos)
   * [Operadores In e range](#operadores-in-e-range)
 * [Manipulando Strings](#manipulando-strings)
   * [Indexação](#indexação)
   * [Concatenação](#concatenação)
   * [Formatação](#formatação)
 * [Diferença entre Empty e Blank](#diferença-entre-empty-e-blank)
   * [Empty x Blank](#empty-e-blank)
 * [Introdução a funções](#introduçao-a-funções)
   * [Funções](#funções)
 * [Funções de Ordem Superior](#funções-de-ordem-superior)
 * [Funções single-line e Funcões/extensões](#funções-single-line-e-funções-/-extensões)
   * [Funções single-line](#funções-single-line)
   * [Funções / Extensões](#funções-/-extensões)
 * [Estruturas de Controle](#estruturas-de-controle)
 * [Atribuições, when e Elvis Operator](#)
   * [Atribuição](#atribuição)
   * [When](#when)
   * [Elvis Operator](#elvis-operator)
 * [Estrutura de Repetição](#estrutura-de-repetição)
   * [For](#for)
 * [Proposta de Exercício final](#proposta-de-exercício-final)

</div>


## Ferramentas

* [Kotlin Playground ( IDE Online )](https://play.kotlinlang.org/)
* [Android Studio ( IDE )](https://developer.android.com/)

## Tipos de Dados

| Tipo de Dado | Conversão  | Valor Máximo               |
| ------------ | ---------- | -------------------------- |
| Int          | toInt()    | println( Int.MAX_VALUE )   |
| Long         | toLong()   | println( Long.MAX_VALUE )  |
| Float        | toFloat()  | println( Float.MAX_VALUE ) |
| Double       | toDouble() |                            |
| Array        |            |                            |
| Boolean      |            |                            |
| Char         | toChar()   |                            |
| Byte         | toByte()   | println( Byte.MAX_VALUE )  |
| Short        | toShort()  | println( Short.MAX_VALUE ) |
| Null!        |            |                            |

## Declaração de Variáveis

* **var** ( valor mutável, CamelCase)
  * Variável que pode ter seu valor alterado durante o código;
* **val** (valor imutável, CamelCase)
  * Variável que terá somente o valor atribuído ( Similar ao final do Java );
* **const val** (valor imutável, SNAKE_CASE)
  * Constante cujo valor é atribuido durante compilação.

### Formas de declaração de Variáveis 

```kotlin
// var ( camelCase ) valor definido e alterado durante a execução 
// Passando o valor direto para a variável
var currentAge = 22
// atribuindo o tipo e / ou nulabilidade utilizando o ?
var currentAge:Int?
// currentAge = null ou 22

// val ( camelCase ) valor definido durante a execução
// Passando o valor direto para a variável
val currentAge = 22
// atribuindo o tipo e / ou nulabilidade utilizando o ?
val currentAge:Int?
// currentAge = null ou 22

// const val ( SNAKE_CASE ) valor definido durante a compilação
const val MIN_AGE = 16
const val MAX_AGE = 68

```
> Atenção : 
* Uma variável **não pode** ser declarada **sem tipo e sem atribuição**
* Uma **variável com inferência de tipo** só receberá **valores do mesmo tipo que a sua primeira atribuição**

## Valor nulo e Operadores aritméticos básicos

### Nullability

* Qualquer tipo pode ser nulo, porém isso deve ser explicitado na declaração de variável através do uso da interrogação ( ? )
* A inferência de tipo não atribui nullability;

```kotlin
var month:Int? = null
// Atribui valor null corretamente a month

var day:Int = null
/* 
  ERRO de compilação 
  Null can not be a value of a non-null type Int
 */
```

###  Operadores Aritméticos 

* Os operadores podem ser chamados tanto como expressão quanto como comando. O resultado será o mesmo.
* A função de soma também serve para concatenar Strings;

| Função        | Expressão | Comando    | Atribuição |
| ------------- | --------- | ---------- | ---------- |
| soma          | a + b     | a.plus(b)  | a += b     |
| subtração     | a - b     | a.minus(b) | a -= b     |
| multiplicação | a * b     | a.times(b) | a *= b     |
| divisão       | a / b     | a.div(b)   | a /= b     |
| resto         | a % b     | a.mod(b)   | a%=b       |

## Operadores Comparativos 

* Os comandos **compareTo** retornam os valores :
  * -1 ( menor que )
  *  0 ( igual )
  *  1 ( maior )
  * Já os **operadores** retornam **valores booleanos**;
* O comando **equals** retorna um **booleano**;

| Função              | Expressão            | Comando                                        |
| ------------------- | -------------------- | ---------------------------------------------- |
| maior / menor       | a > b **ou** a < b>  | a.compareTo(b) > 0 **ou** a.compareTo(b) < 0   |
| maior / menor igual | a >= b **ou** a <= b | a.compareTo(b) >= 0 **ou** a.compareTo(b) =< 0 |
| igual               | a == b               | a.equals(b)                                    |
| diferente           | a != b               | !(a.equals(b))                                 |

```kotlin
// Exemplo : 
const val EQUAL = 0
const val LESS = -1
const val MORE = 1
// função principal
fun main() {
  val d = 22
  val t = 90

  println(d > t) // false
  println(d.compareTo(t)) // -1
  println(d.compareTo(t) == MORE) // false 

} 

```
## Operadores lógicos e Operadores In e Range 

### Operadores Lógicos 

* Quando utiliza-se o comando, é recomendado colocar a expressão entre parênteses;

| Função e expressão | Comando                       |
| ------------------ | ----------------------------- |
| E ( **&&** )       | (expressão1) and (expressão2) |
| Ou (**\|\|**)      | (expressão1) or (expressão2)  |


### Operadores **In** e **range** 

* Se valor está presente em uma lista ou uma faixa (range) de valores;
* Range cria um intervalo de valores que inicia no primeiro parâmetro e acaba no segundo.


| Função e expressão                    |
| ------------------------------------- |
| contém (**In**)                       |
| Não contém (**!In**)                  |
| range/Faixa de valores (**Int..Int**) |

```kotlin
// Exemplo 1 : 
val numbers = listOf(3,9,0,1,2)
print(12 in numbers)
// false

print( 12 in 0..20 )
//true

```
```kotlin
// Exemplo 2 : 
fun main() {
  val bingo = listOf(8,6,34,2,13)
  var number = 34

  println(number)  // 34
  println(number in bingo) // true
}

```
```kotlin
// Exemplo 3 : 
const val MIN_AGE = 16
const val MAX_AGE = 68
// Função principal
fun main() {
  var age = (10..100).random()
  println(age) // 46
  println(age in MIN_AGE..MAX_AGE)  //true
  println(age >= MIN_AGE && age <= MAX_AGE>) // true
}
```

## Manipulando Strings 

* Strings possuem diversos métodos associados;
* indexação, concatenação, comparação, formatação;
* Pode ser concatenada com plus +;
* Também é tratada como um array de Char;

### Indexação

* String como array;
* First(), last(), String.lenght, String[index];

```kotlin
val s = "Olá, Mundo!!!"

println(s[0])
println(s.First())
// imprime 0

println(s[s.lenght-1])
println(s.last())
// imprime !

```

### Concatenação

* Para concatenar duas strings o plus + pode ser utilizado;
* Para concatenar uma variável a uma String, os símbolos ${} devem ser inseridos;

```kotlin
val name = "Ana"
val s = "Olá"

println(s+name)
// Imprime OláAna

println("${s}, ${name}!")
// Imprime Olá, Ana!

println("Bem vinda(o), ${name}!")
// Imprime Bem vinda(o), Ana!
```

### Formatação

| Nome                       | função                                                       | Métodos                                                    |
| -------------------------- | ------------------------------------------------------------ | ---------------------------------------------------------- |
| Capitalização de Strings   | Alterar a formatação entre letras maiúsculas e minúsculas    | capitalize(), toUpperCase(), toLowerCase(), decapitalize() |
| Remoção de espaços         | Remove espaços vazios e caracteres inadequados para impresão | trimEnd(), trimStart(), trim()                             |
| Substituição de caracteres | Substituir caracteres por outros                             | replace(x,y)                                               |
| Formatação                 | Formatar outros valores para um padrão de string             | "padrão ${valor}".format(valor)                            |

## Diferença entre Empty e Blank 

### Empty x Blank

* Métodos de comparação;
* String está vazia , em branco ou é nula ?
* Se o **tamanho da string** (s.length) **for 0 está empty e Blank**;
* Se o tamanho **for > 0** mas **todos os caracteres são espaços em branco,** está **blank** mas **não empty**;

```kotlin
// Exemplo 1 : 
val s = ""
println(s.isEmpty())
// true
println(s.isBlank())
// true 
println(s.isNullOrBlank() || s.isNullOrEmpty())
// true
```
```kotlin
// Exemplo 2 : 
val s = "       "
println(s.isEmpty())
// false
println(s.isBlank())
// true 
```
```kotlin
// Exemplo 3 : 
fun main() {
  val empty = ""
  println(empty.lenght)
  // 0
  
  val blank = "              "
  println(blank.lenght)
  // 14

  println(empty.isEmpty() && empty.isBlank())
  // true

  println(blank.isEmpty() && blank.isBlank())
  // false
}
```

## Introdução a funções 

### Funções

* Prefixo **Fun nomeDaFunção(nome:Tipo):TipoRetorno{}**
* Funções anônimas, single-line, inline, extensões, Lambdas, ordem superior;

<h4>Simplificando uma Função</h4>

```kotlin
// Exemplo 1 : 
private fun getFullName(name:String, lastName:String): String{
  val fullname = "$name $lastName"
  return fullname
}

// Exemplo 2 : 
private fun getFullName(name:String, lastName:String): String{
  return = "$name $lastName"
}

// Exemplo 3 : 
private fun getFullName(name:String, lastName:String) = "$name $lastName"
```
## Funções de Ordem Superior 

* Recebem outra função ou lambda por parâmetro;
* Bastante úteis para a generalização de funções e tratamento de erros;

```kotlin
val x = calculate(12,4,::sum)

val x = calculate(12,4){a,b -> a*b }

```

```kotlin
// Exemplo 1 : 
// Função principal
fun main() {
val z:Int

  z = calculate(34,90, ::sum)
  println(z)
}

fun sum(a1:Int, a2:Int) = a1.plus(a2)

fun calculate(n1:Int,n2:Int, operation:(Int,Int)->Int):Int{
    val result = operation(n1,n2)
    return result
}
// 124
```

```kotlin
// Exemplo 2 : 
// Função principal
fun main() {
val z:Int

  z = calculate(34,90){a,b -> a*b}
  println(z)
}

fun sum(a1:Int, a2:Int) = a1.plus(a2)

fun calculate(n1:Int,n2:Int, operation:(Int,Int)->Int):Int{
    val result = operation(n1,n2)
    return result
}
// 3060
```

```kotlin
// Exemplo 3 : 
// Função principal
fun main() {
val z:Int

  z = calculate(34,90){a,b -> 
  println("vou calcular")
     a+b
  }
  println(z)
}

fun sum(a1:Int, a2:Int) = a1.plus(a2)

fun calculate(n1:Int,n2:Int, operation:(Int,Int)->Int):Int{
    val result = operation(n1,n2)
    return result
}
// vou calcular 124
```

```kotlin
// Exemplo 4 : 
// Função principal
fun main() {

  calculate(34, 90){a,b -> 
  println("vou calcular! $a + $b")
  }
}

fun sum(a1:Int, a2:Int) = a1.plus(a2)

fun calculate(n1:Int,n2:Int, operation:(Int,Int)->Unit){
    val result = operation(n1,n2)
}
// vou calcular! 34 + 90
```

```kotlin
// Exemplo 5 : 
// Função principal
fun main() {
val z:Int

  z = calculate(34,90){a,b -> 
  println("vou calcular! $a + $b * 2 ")
  (a+b) * 2
  }
  println(z)
}

fun sum(a1:Int, a2:Int) = a1.plus(a2)

fun calculate(n1:Int,n2:Int, operation:(Int,Int)->Int)= operation(n1,n2)
/* vou calcular! 34 + 90 * 2 
248
*/ 
```

## Funções single-line e Funcões/extensões 

### Funções single-line

* Prefixo **Fun nomeDaFuncao(nome:Tipo) = retorno**;
* Função de uma única linha;
* Infere o tipo de retorno;

```kotlin
private fun getFullname(name:String, lastName:String) = "$name $lastName"
```

### Funções / Extensões

* Prefixo **Fun Tipo.nomeDaFuncao()**;
* Cria uma função que só pode ser chamada por um tipo específico, cujo o valor pode ser referenciado dentro da função através da palavra **this**;

```kotlin
fun String.randomCapitalizedLetter() = 
    this[(0..this.length-1).random()].toUpperCase()

```

## Estruturas de Controle 

* if/else, when, elvis operator;
* Pode ser utilizado tanto para controle quanto para atribuição;
* Pode ser encadeado com múltiplas estruturas;

``` kotlin
// Exemplo if
if(expressão1){
  // bloco de código
} else if(expressão2){
  // bloco de código
} else {
  // bloco de código
}
```

``` kotlin
// Exemplo when
when {
  case1 -> {}
  case2 -> {}
  case3 -> {}
  else  -> {}
}
```

``` kotlin
// Exemplo elvis operator
val a:Int? = null
var number = a ?: 0
```

## Atribuições, when e Elvis Operator 

### Atribuição

* O valor atribuído tem que estar na ultima linha do bloco;
* A condicional pode não usar chaves se só fizer a atribuição

```kotlin
val MaxValue = if(a > b) a else if (a < b) b else b

val MinValue = if (a > b) {
  println("b($b é o menor valor")
  b
} else if(a < b) {
  println("a($a) é o mernor valor")
  a
}else{
  println("os valores são iguais")
  b
}
```

### When

* Equivalente ao switch de outras linguagens;
* Aceita tanto valores quando condicionais
* Aceita range como case;

```kotlin
// Modelo : 
when {
  a > b -> {}
  a != b && a > c -> {}
  b == 0 -> {}
  else -> {}
}
```

```kotlin
// Exemplo 1 : 
when(year) {
  -4000..475 -> "Antiguidade"
  476..1452 ->  "Medieval"
  1453..1789 ->  "Moderna"
  currentYear -> "Ano atual"
}
```
```kotlin
// Exemplo 2 : 
// Função principal
fun main() {
val grade = (0..10).random()
    println(grade.getStudentStatus())
}

fun Int.getStudentStatus(): String{
    println("Nota $this")
    return when(this){
        in 0..4 -> "Reprovado"
        in 5..7 -> "Mediano"
        in 8..9 -> "Bom"
        10 -> "Excelente"
        else -> "Nota não Lançada!"
    }
}
```

### Elvis Operator

* O mais próximo que a linguagem possui de um operador ternário;
* verifica se um valor é nulo e apresenta uma opção caso seja;
* Pode ser encadeado;

```kotlin
val a:Int? = null
val c:Int? = 9
val number = a?: b?: 0 
```
* Se o valor de **a não for nulo**, number **recebe a**. 
* Se o valor de **a for nulo** e **b não for nulo**, number **recebe b**, se **a e b forem nulos**, number **recebe 0**. 

```kotlin
// Exemplo 1 : 
// Função principal
fun main() {

    var t:Int
    var x:Int? = 10
    var w:Int? = null
    t = x?:w?: -1 
    
    println(t)
}
// 10
```
```kotlin
// Exemplo 2 : 
// Função principal
fun main() {

    var x:Int? = 10
    var t:String? = x?.getStudentStatus() 
    
    println(t)
}

fun Int.getStudentStatus(): String{
    println("Nota $this")
    return when(this){
        in 0..4 -> "Reprovado"
        in 5..7 -> "Mediano"
        in 8..9 -> "Bom"
        10 -> "Excelente"
        else -> "Nota não Lançada!"
    }
}
// Nota 10
// Excelente
```

## Estrutura de Repetição 

* While, do..while, for e forEach
* Estruturas similares às convencionais em outras linguagens;
* Aceita os comandos **in, range, until, downTo e step**;

```kotlin
while(Condição){

}
```

```kotlin
do{
  // bloco de código
} while(Condição)
```

```kotlin
for(i in 0..20 step 2){
  println(i)
} 
```

### For

* **for**(variavel_indexadora **in/until/downTo** faixa_de_valores/condicional **step** intervalo)
* **In**: conta do valor inicial até o valor final estabelecido;
* **Until**: conta do valor atual até o valor estabelecido meno 1;
* **DownTo**: conta de maneira decrescente;
* **Step**: determina o intervalo da contagem;

```kotlin
for(i in 0..20 step 2){
  println(i)
}

for( i in 10 downTo 0 ) {
  println(i)
}

for(i in 0 until 10) {
  println(i)
}

var text = "Kotlin"
for(letter in text){
  println(letter)
} 
```

```kotlin
// função principal
fun main(){
  for(i in 0..20){
    print("$i ")
  }
}
// 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20

fun downTo(){
  for(i in 20 downTo 0){
    print("$i ")
  }  
}
// 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0 

fun until(){
  for(i in 1 until 20){
    print("$i ")
  }  
}
// 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19

fun step(num:Int){
   for(i in 0..20 step num){
    print("$i ")
  } 
}
// step(2)
// 0 2 4 6 8 10 12 14 16 18 20 

fun letters(){
   val sArray = "Olha essa string!"
   for(l in sArray){
     println(l.toUpperCase())
   }
}
/*

O
L
H
A
 
E
S
S
A
 
S
T
R
I
N
G

*/
```

## Proposta de Exercício final 

Com os conhecimentos aplicados anteriormente vamos criar uma **calculadora** que dado **dois valores Float?** e **um número corresponde a operação** ( constante ), retorne e **imprima o resultado como Float** ou uma **mensagem de erro caso um dos valores seja nulo**.
